const {
  includes,
} = require('lodash');

const cleanupGermanCategory = (category) => {
  switch (category) {
    case 'Stäucher':
      return 'Sträucher';
    case 'container':
      return 'Container';
    case 'bare root':
      return 'Wurzelnackt';
    default:
      return category;
  }
}

const cleanupEnglishCategory = (category) => {
  switch (category) {
    case 'container':
    case 'Container':
    case 'potted':
      return 'Potted';
    case 'shrubs':
      return 'Shrubs';
    case 'root ball':
      return 'Root Ball';
    case 'bare root':
      return 'Bare Root';
    case 'clear stem':
      return 'Clear Stem';
    default:
      return category;
  }
}

const cleanupRussianCategory = (category) => {
  switch (category) {
    default:
      return category;
  }
}

const getArticleCategories = (qualitiesCategories) => {
  const catSep = ' / '; // Category separator

  const articleCategories = {};
  qualitiesCategories.forEach((qualityCategories, index) => {
    const splitCategories = qualityCategories['de'] && qualityCategories['de'].split(catSep) || []; // Using German names to determine uniqueness
    
    splitCategories.forEach((germanCategory, index) => {
      let englishCategory = qualityCategories['en'] && qualityCategories['en'].split(catSep)[index];
      let russianCategory = qualityCategories['ru'] && qualityCategories['ru'].split(catSep)[index];

      // Clean up category names to make sure no mistakes
      germanCategory = cleanupGermanCategory(germanCategory);
      englishCategory = cleanupEnglishCategory(englishCategory);
      russianCategory = cleanupRussianCategory(russianCategory);

      // Init category if doesn't exist...
      if (!articleCategories[germanCategory]) {
        articleCategories[germanCategory] = {
          names: [],
          qualities: []
        };
      }
      // Add language if doesn't exist and we have it... not doing this in initialize because don't want spreadsheet fuck ups to break everything
      if (germanCategory && !includes(articleCategories[germanCategory].names, germanCategory)) {
        articleCategories[germanCategory].names.push(germanCategory);
      }
      if (englishCategory && !includes(articleCategories[germanCategory].names, englishCategory)) {
        articleCategories[germanCategory].names.push(englishCategory);
      }
      if (russianCategory && !includes(articleCategories[germanCategory].names, russianCategory)) {
        articleCategories[germanCategory].names.push(russianCategory);
      }

      // Add quality to category
      articleCategories[germanCategory].qualities.push(qualityCategories.quality);
    });
  });

  // console.log('articleCategories, s',articleCategories, 'articleCategories, e');

  const articleCategoriesByLang = [];

  // Prepare DB objects...
  const keys = Object.keys(articleCategories);
  keys.forEach(cat => {
    articleCategories[cat].names.forEach((name, index) => {
      articleCategoriesByLang.push({
        deleted: false,
        hidden: false,
        lang: index, // Index corresponds to the lang, which is why we add DE then EN then RU in order above
        name,
        qualities: articleCategories[cat].qualities,
      });
    });
  });

  // Generate ids, maybe we could move this to back-end instead when doing language stuff
  articleCategoriesByLang.map((cat, index) => {
    cat.id = index + 1;
    return cat;
  });

  // console.log('articleCategoriesByLang, s',articleCategoriesByLang, 'articleCategoriesByLang, e');

  return articleCategoriesByLang;
};

const setArticlesCategoriesMM = (articles, articleCategories) => {
  const articlesCategoriesMM = [];
  // Populate all article to category relationships
  articles.forEach(article => {
    articleCategories.forEach(category => {
      // console.log(category);
      if (article.lang === category.lang && category.qualities.indexOf(article._qualityde) > -1) {
        articlesCategoriesMM.push({
          articleId: article.id,
          categoryId: category.id
        });
      }
    });
  });
  // Generate IDs for the table rows
  articlesCategoriesMM.map((cat, index) => {
    cat.id = index + 1;
    return cat;
  });
  return articlesCategoriesMM;
};

module.exports = {
  setArticlesCategoriesMM,
  getArticleCategories
};