
String.prototype.trimWhiteSpace = function () {
  return this.replace(/ +(?= )/g,'');
};

const getArticles = (plants, lang) => {
  if (!plants) {
    return new Error('Can\'t find any plants');
  }
  const articles = [];
  plants.forEach(plant => {
    const hasArticle = articles.find(article => article.oid === plant.winarbor_artikelnummer);

    const hasPrice = plant.netto1 && plant.netto1 > 0 ? true : false;
    const quality = (lang === 1 && plant.qualitaet_en) || 
      (lang === 2 && plant.qualitaet_ru) || 
      plant.qualitaet_de;
    const hasQuality = quality && quality.length > 0 ? true : false;
    
    if (!hasArticle && hasPrice && hasQuality) {

      const article = {
        oid: plant.winarbor_artikelnummer,
        sortOid: plant.bdb_sortentext_nr,
        generaOid: plant.gattung_nr,
        lang: lang,
        bdbnr: plant.bdbnr,
        matchcode: plant.artikel_matchcode,
        deleted: false,
        hidden: !plant.web,
        quality: quality.replace(/ +(?= )/g,''),
        minwidth: plant.breite_min,
        maxwidth: plant.breite_max,
        minheight: plant.groesse_min,
        maxheight: plant.groesse_max,
        mintreetrunkscope: plant.stammumfang_min,
        maxtreetrunkscope: plant.stammumfang_max,
        clenchmin: plant.ballen_min,
        clenchmax: plant.ballen_max,
        price1: plant.netto1,
        price10: plant.netto2,
        minqty: plant.mindestens,
        quality_recommendation: plant.empfehlung,
        price_recommendation: plant.preis_ab,
        shop_category1: plant.shop1,
        shop_category2: plant.shop2,
        shop_category3: plant.shop3,
        shop_category4: plant.shop4,
        articleCategoryId: null,
        winarborId: plant.winarbor_artikelnummer,
        _qualityde: plant.qualitaet_de.replace(/ +(?= )/g,'')
      };
      articles.push(article);
    }
  });
  return articles;
};

const setArticleIds = (articles, sorts) => {
  if (!sorts || !articles) {
    if (!sorts) {
      return new Error('Can\'t find any sorts');
    }
    if (!articles || articles && articles.length === 0) {
      return new Error('Can\'t find any articles');
    }
  }

  articles.map((article, index) => {
    const sort = sorts.find(s => s.oid === article.sortOid && s.lang === article.lang);
    if (sort) {
      articles[index].id = index + 1;
      articles[index].sortId = sort.id;
      delete articles[index].sortOid;
    }
  });

  return articles;
};

module.exports = {
  getArticles,
  setArticleIds
}