const options = require('./options');

const {
  assignTimeStamps,
} = require('./utils');

const getArts = (plants, descriptions, lang) => {
  if (!plants) {
    return new Error('Can\'t find any plants');
  }
  const arts = [];
  plants.forEach(plant => {
    const hasArt = arts.find(art => art.oid === plant.gattung_art_nr);
    const description = descriptions && descriptions.find(descr => descr.bdb_sorten_nr === plant.bdb_sortentext_nr && descr.sprache === options.availableLanguages[lang]);

    const name1 = 
      (description && description.landesname) ||
      (lang === 0 && plant.landesname_de) || 
      (lang === 1 && plant.landesname_en) || 
      (lang === 2 && plant.landesname_ru) || 
      plant.gattung_art;

    if (!hasArt && plant.gattung_art_nr) {
      const art = {
        oid: plant.gattung_art_nr,
        name1,
        deleted: false,
        lang,
        __generaOid: plant.gattung_nr,
      };
      const isHidden = () => {
        if (
          !plant.web || 
          (!art.name1 || art.name1 && art.name1.length === 0)
        ) { return true; }
        return false;
      };
      art.hidden = isHidden();
      arts.push(assignTimeStamps(art));
    }
  });
  return arts;
};

const setArtIds = (arts, generas) => {
  if (!generas) {
    return new Error('Can\'t find any genera');
  }
  if (!arts) {
    return new Error('Can\'t find any arts');
  }
  arts.map((art, index) => {
    const genera = generas.find(g => g.mapping.indexOf(art.__generaOid) > -1 && g.lang === art.lang);
    if (genera) {
      arts[index] = Object.assign({}, art, {
        id: index + 1,
        generaId: genera.id,
        hidden: genera.hidden || art.hidden,
      });
      delete arts[index].__generaOid;
    }
  });
  
  return arts;
};

module.exports = {
  getArts,
  setArtIds,
};