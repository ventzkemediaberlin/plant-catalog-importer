const csvjson = require('csvjson');
const fs = require('fs')
const path = require('path');
const axios = require('axios')
const Bottleneck = require('bottleneck');

const limiter = new Bottleneck({
    minTime: 200,
    maxConcurrent: 5
});




'use strict';

//TODO ADD succesful & failed count

const createSan = () => {

    const data = fs.readFileSync(path.join(__dirname, '..', 'dist', 'formen.csv'), { encoding: 'utf8' });

    var options = {
        delimiter: ';', 
        quote: '"' 
    };

    let succesfulCount = 0;
    let failedCount = 0;


    const json = csvjson.toObject(data, options);

    const getData = (item) => {
        let uri = encodeURI(`http://localhost:1338/api/sorts?filter={"where":{"matchcode":"${item['SORTEN_MATCHCODE']}"}}`)

        const res = axios.get(uri)
            .then(res => {
                    getSortMediaImage(res.data, item)             
            })
            .catch(err => {
                console.log('err 1', err)
            })
    }

    const throttledGetData = limiter.wrap(getData);

    async function getAllResults() {

        const allThePromises = json.map(item => {
            if (item['BILD'].length !== 0) {
                throttledGetData(item)
            }
        })


        try {
            const results = await Promise.all(allThePromises);
        }
        catch (err) {
            console.log('err 2', err);
        }
    }




    const getSortMediaImage = (data, item) => {

        data = data.rows[0];

      const regex = /[!@#$%^&*(),?'":{}| <>]/g;
         while ((regex.exec(item["BILD"])) !== null) {
            item["BILD"] = item["BILD"].replace(regex, '_');
      }

      item["BILD"] = item["BILD"].replace(/JPG/g, 'jpg');
      

      let uri = encodeURI(`http://localhost:1338/api/sorts/${data.id}/mediaImages?filter={"where":{"filename":"${item["BILD"]}"}}`)

        const res = axios.get(uri)
        .then(res => {
            if (!res.data[0]) {
                postCall(item, data)
            }
        })
        .catch(err => {
            console.log('err 3', err)
        })
    }

    getAllResults();

    const postCall = (item, data) => {

        // define data + right URI
        console.log('postItem', item.BILD)
        console.log('postData', data)

        date = new Date;

        let postDataObj = {
            active: true,
            alternateText: null,
            deleted: false,
            filename: item.BILD,
            url: `images/forms/${item.BILD}`,
            tstmp: date,
            crdate: date
        }

        let uri = encodeURI(`http://localhost:1338/api/sorts/${data.id}/mediaImages`)
                const postData = async () => {
                    try {
                        const res = await axios.post(uri, postDataObj)
                            .then(res => {
                                console.log('post responce', res.status)
                            })
                    } catch (err) {
                        console.log('post err', err)
                        failedCount++
                    }
                }

                postData();

    }

    console.log('succesfulCount', succesfulCount)
    console.log('failedCount', failedCount)

}



createSan();


