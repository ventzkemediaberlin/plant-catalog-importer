const {
  includes,
} = require('lodash');

const generaCategoryLabels1 = [
  ['German', 'English', 'Russian'], // Will never be hit because shop categories index at 1
  // Column 1
  {
    slug: 'laub',
    order: 1,
    names: ['Laub', 'Leaves', 'Уходит'],
    hidden: false
  },
  {
    slug: 'nadel',
    order: 2,
    names: ['Nadel', 'Needle', 'игла'],
    hidden: false
  },
  {
    slug: 'obst',
    order: 3,
    names: ['Obst', 'Fruit', 'Фрукты'],
    hidden: false
  },
  {
    slug: 'stauden',
    order: 4,
    names: ['Stauden', 'Perennials', 'Многолетники'],
    hidden: false
  },
  {
    slug: 'mediteran',
    order: 4,
    names: ['Mediterrane', 'Mediterranean', 'Средиземное море'],
    hidden: true
  },
  {
    slug: 'azaleen',
    order: 5,
    names: ['Rhododendron', 'Rhododendron', 'Рододендрон'],
    hidden: false
  },
  {
    slug: 'rosen',
    order: 6,
    names: ['Rosen', 'Roses', 'Розы'],
    hidden: false
  },
  {
    slug: 'bambus',
    order: 7,
    names: ['Bambus', 'Bambus', 'Bambus'],
    hidden: false
  },
  {
    slug: 'resista',
    order: 8,
    names: ['Resista', 'Resista', 'Resista'],
    hidden: false
  },
  {
    slug: 'formen',
    order: 12,
    names: ['Formen', 'Shapes', 'Формы'],
    hidden: false
  },
  {
    slug: 'zubehoer',
    order: 12,
    names: ['Zubehör', 'Equipment', 'Оборудование'],
    hidden: true
  }
];

const generaCategoryLabels2 = [
  ['German', 'English', 'Russian'],
  // Column 2
  {
    slug: 'baeume',
    order: 9,
    names: ['Hochstämme', 'Clear Stem', 'Очистить Стебель'],
    hidden: false
  },
  {
    slug: 'solitaer',
    order: 10,
    names: ['Solitär', 'Solitaire', 'Пасьянс'],
    hidden: false
  },
  {
    slug: 'hecken',
    order: 11,
    names: ['Hecken', 'Hedges', 'Хеджирование'],
    hidden: false
  },
  {
    slug: 'bonsai',
    order: 13,
    names: ['Bonsai', 'Bonsai', 'карликовое дерево'],
    hidden: false
  },
  {
    slug: 'climbing',
    order: 13,
    names: ['Kletterpflanzen', 'Climbing Plants', 'Вьющиеся растения'],
    hidden: true
  }
];

const getGeneraCategories = (generas, articles, lang) => {
  // Prepare DB objects...
  let generaCategories = [];
  
  // Find the articles for the generas, since the articles are the ones with the shop1/shop2 assigned to them atm
  articles.forEach(article => {
    generas.forEach(genera => {
      if (includes(genera.mapping, article.generaOid) && article.lang === genera.lang) {
        // Looking at shop_category1 for the article, and populating the genera categories
        if (article.shop_category1 && generaCategoryLabels1[article.shop_category1]) {
          const category = generaCategoryLabels1[article.shop_category1];
          generaCategories = injectCategoryIntoCategories(generaCategories, category, article, lang);
        }

        if (article.shop_category2 && generaCategoryLabels2[article.shop_category2]) {
          const category = generaCategoryLabels2[article.shop_category2];
          generaCategories = injectCategoryIntoCategories(generaCategories, category, article, lang);
        }

        const formenAdditionalCategories = [3, 4, 5, 6, 7, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48];
        if (article.shop_category2 && includes(formenAdditionalCategories, article.shop_category2)) {
          const category = generaCategoryLabels1[10];
          generaCategories = injectCategoryIntoCategories(generaCategories, category, article, lang);
        }
      }
    });
  });

  // Have to prepare again...
  const generaCategoriesByLang = [];
  // Prepare DB objects...
  const keys = Object.keys(generaCategories);
  keys.forEach(cat => {
    generaCategoriesByLang.push({
      deleted: false,
      hidden: generaCategories[cat].hidden,
      order: generaCategories[cat].order,
      lang,
      name: generaCategories[cat].name,
      slug: generaCategories[cat].slug,
      generas: generaCategories[cat].generas,
    });
  });

  return generaCategoriesByLang;
};

const setGeneraCategoriesIds = (generaCategories) => {
  if (!generaCategories) {
    return new Error('Can\'t find any genera categories');
  }
  
  generaCategories.map((cat, index) => {
    cat.id = index + 1;
    return cat;
  });
  return generaCategories;
};

const injectCategoryIntoCategories = (categories, category, article, lang) => {
  const slug = category.slug;
  const name = category.names[lang];
  const hidden = category.hidden;
  const order = category.order;

  // Initialize object if not already done
  if (!categories[slug]) {
    categories[slug] = {
      deleted: false,
      order,
      hidden,
      lang,
      name,
      slug,
      generas: [],
    };
  }

  // Add genera if not present
  if (!includes(categories[slug].generas, article.generaOid)) {
    categories[slug].generas.push(article.generaOid);
  }

  return categories;
}

const setGenerasCategoriesMM = (generas, generaCategories) => {
  const generasCategoriesMM = [];
  // Populate all article to category relationships
  generas.forEach(genera => {
    generaCategories.forEach(category => {
      if (genera.lang === category.lang && includes(category.generas, genera.oid)) {
        generasCategoriesMM.push({
          generaId: genera.id,
          categoryId: category.id
        });
      }
    });
  });
  // Generate IDs for the table rows
  generasCategoriesMM.map((cat, index) => {
    cat.id = index + 1;
    return cat;
  });
  return generasCategoriesMM;
};

module.exports = {
  setGenerasCategoriesMM,
  getGeneraCategories,
  setGeneraCategoriesIds
};