const options = require('./options');

const {
  assignTimeStamps,
} = require('./utils');

const {
  findIndex,
} = require('lodash');

const getGenera = (plants, descriptions, lang) => {
  if (!plants) {
    return new Error('Can\'t find any plants');
  }
  
  const generas = [];

  plants.forEach(plant => {
    const hasGenera = generas.find(genera => genera.oid === plant.gattung_nr);
    const description = descriptions && descriptions.find(descr => descr.bdb_sorten_nr === plant.bdb_sortentext_nr && descr.sprache === options.availableLanguages[lang]);
    
    const name1 = 
      plant.gattung ||
      (description && description.bot_name_katalog);
    
    const name2 = 
      (lang === 0 && plant.landesname_de) || 
      (lang === 1 && plant.landesname_en) || 
      (lang === 2 && plant.landesname_ru) || 
      plant.gattung_art ||
      (description && description.landesname);


    if (!hasGenera || hasGenera.length === 0) {
      const genera = {
        oid: plant.gattung_nr,
        name1,
        name2,
        lang,
        deleted: false,
        hidden: !plant.web,
      };
      const isHidden = () => {
        if (
          !plant.web || 
          (!genera.name1 || genera.name1 && genera.name1.length === 0) ||
          (!genera.name2 || genera.name2 && genera.name2.length === 0)
        ) { return true; }
        return false;
      };
      genera.hidden = isHidden();
      generas.push(assignTimeStamps(genera));
    }
  });
  return generas;
};

const setGeneraIds = (generas) => {
  if (!generas) {
    return new Error('Can\'t find any genera');
  }
  generas.map((genera, index) => {
    generas[index].id = index + 1;
  });
  return generas;
};

const findDuplicates = (generas) => {
  if (!generas) {
    return new Error('Can\'t find any genera');
  }
  const uniqGenera = [];
  generas.map(genera => {
    const duplicate = uniqGenera.find(g => g.name1 === genera.name1 && g.lang === genera.lang);
    if (!duplicate) {
      genera.mapping = [genera.oid];
      uniqGenera.push(genera);
    } else {
      const index = findIndex(uniqGenera, g => g.oid === duplicate.oid);
      if (index > -1) {
        uniqGenera[index].mapping.push(genera.oid);
      }
    }
  });
  return uniqGenera;
};

module.exports = {
  findDuplicates,
  getGenera,
  setGeneraIds,
};