const path = require('path');
var sizeOf = require('image-size');

/**
  * IMAGES CATEGORYY TYPES
  * 1 = Default Product image
  * 2 = Blossom image
  * 3 = Habitus image
  * 4 = Bark image
  * 5 = Quality image
  * 6 = Usage image
  * 7 = treetrunk image
  * 8 = fruit
  * 9 = genera
  * 10 = leaf
  */

const IMAGE_TYPES = {
  bark: {
    id: 4,
    key: 'BARK',
  },
  blossom: {
    id: 2,
    key: 'BLOSSOM',
  },
  habitus: {
    id: 3,
    key: 'HABITUS',
  },
  fruit: {
    id: 8,
    key: 'FRUIT',
  },
  genera: {
    id: 9,
    key: 'GENERA',
  },
  leaf: {
    id: 10,
    key: 'LEAF',
  },
  product: {
    id: 1,
    key: 'PRODUCT',
  },
  quality: {
    id: 5,
    key: 'QUALITY',
  },
  trunk: {
    id: 7,
    key: 'TREETRUNK',
  },
  usage: {
    id: 6,
    key: 'USAGE',
  },
};

const getImageType = file => file ?
  path.basename(path.dirname(file)) : null;

const getImages = (files) => {
  if (!files) {
    return new Error('Can\'t find any Image');
  }
  const images = [];
  files.forEach((file, index) => {
    const filename = path.basename(file);
    const type = getImageType(file);
    const extName = path.extname(file);
    const dimensions = sizeOf(file);
    images.push({
      active: true,
      alternateText: null,
      deleted: false,
      description: null,
      fileExtension: extName.replace('.', ''),
      filename,
      height: dimensions.height,
      id: index + 1,
      imageType: IMAGE_TYPES && IMAGE_TYPES[type] && IMAGE_TYPES[type].id || null,
      imageTypeKey: IMAGE_TYPES && IMAGE_TYPES[type] && IMAGE_TYPES[type].key || null,
      name: null,
      source: filename.replace(extName, ''),
      url: `images/${type}/${filename}`,
      width: dimensions.width,
      __matchCodes: filename.replace(extName, '').split('_'),
    });
  });
  return images;
};

const sortImageMapping = (data) => {
  if (!data) {
    return new Error('No data provided');
  }
  const images = data[0];
  const sorts = data[1];
  if (!sorts) {
    return new Error('No sorts provided');
  }
  if (!images) {
    return new Error('No images provided');
  }
  const mapping = [];
  let index = 1;
  sorts.forEach(sort => {
    const relatedImages = images.filter(img => img.__matchCodes && img.__matchCodes.indexOf(sort.matchcode) > -1);
    if (relatedImages && relatedImages.length > 0) {
      relatedImages.forEach((img) =>{ 
        mapping.push({
          id: index + 1,
          mediaImageId: img.id,
          sortId: sort.id
        });
        index++;
      });
    }
  });
  return mapping;
};

module.exports = {
  getImages,
  sortImageMapping,
};