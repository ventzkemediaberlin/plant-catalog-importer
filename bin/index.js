const {
  globAsync,
  promise,
} = require('./utils');

const options = require('./options');

const {
  findDuplicates,
  getGenera,
  setGeneraIds,
} = require('./genera');

const {
  getArts,
  setArtIds,
} = require('./arts');

const {
  assignMinMaxValues,
  getSorts,
  setSortIds,
} = require('./sorts');

const {
  getArticles,
  setArticleIds
} = require('./articles');

const {
  getImages,
  sortImageMapping,
} = require('./images');

const {
  getArticleCategories,
  setArticlesCategoriesMM,
} = require('./article-categories');

const {
  getGeneraCategories,
  setGenerasCategoriesMM,
  setGeneraCategoriesIds
} = require('./genera-categories');

class PlantImport {

  /**
   * 
   */
  constructor(props = {}) {
    this.props = Object.assign({}, options, props);
    this.precalced = {};
  }

  /**
   */
  get articles() {
    if (this.precalced.articles) return this.precalced.articles;
    let articles = [];

    for (let lang = 0; lang < this.availableLanguages.length; lang++) {
      articles = articles.concat(getArticles(this.plants, lang));
    }
    articles = setArticleIds(articles, this.sortsWithoutMinMax);

    this.precalced.articles = articles;
    return articles;
  }

  get articleCategories() {
    if (this.precalced.articleCategories) return this.precalced.articleCategories;
    let articleCategories = getArticleCategories(this.plantQualityCategories, this.qualities);

    this.precalced.articleCategories = articleCategories;
    return articleCategories;
  }

  get articleCategoriesAsync() {
    let articleCategories = JSON.parse(JSON.stringify(this.articleCategories));
    articleCategories = articleCategories.map(cat => {
      delete cat.qualities;
      return cat;
    });
    return promise(articleCategories);
  }

  get articlesCategoriesMM() {
    if (this.precalced.articlesCategoriesMM) return this.precalced.articlesCategoriesMM;
    // console.log('this.articleCategories, s', this.articleCategories, 'this.articleCategories, e');
    let articlesCategoriesMM = setArticlesCategoriesMM(this.articles, this.articleCategories);

    this.precalced.articlesCategoriesMM = articlesCategoriesMM;
    return articlesCategoriesMM;
  }

  get articlesCategoriesMMAsync() {
    return promise(this.articlesCategoriesMM);
  }

  get articlesAsync() {
    return promise(this.articles);
  }

  get arts() {
    if (this.precalced.arts) return this.precalced.arts;
    let arts = [];

    for (let lang = 0; lang < this.availableLanguages.length; lang++) {
      arts = arts.concat(getArts(this.plants, this.descriptions, lang));
    }
  
    arts = setArtIds(arts, this.genera);
    this.precalced.arts = arts;
    return arts;
  }

  get artsAsync() {
    return promise(this.arts);
  }

  get availableLanguages() {
    const availableLanguages = [];
    this.plantProps.forEach(prop => {
      const lang = prop.sprache;
      if (lang && availableLanguages.indexOf(lang) === -1 && this.props.availableLanguages.indexOf(lang) > -1) {
        availableLanguages.push(lang);
      }
    });
    return availableLanguages;
  }

  get data() {
    return Promise.all([
      this.generaAsync,
      this.artsAsync,
      this.sortsAsync,
      this.articlesAsync,
      this.images,
      this.sortImageMapping,
      this.articleCategoriesAsync,
      this.articlesCategoriesMMAsync,
      this.generaCategoriesAsync,
      this.generasCategoriesMMAsync,
    ]).then(data => ({
      genera: data[0],
      art: data[1],
      sort: data[2],
      article: data[3],
      media_image: data[4],
      sort_mediaimage_mm: data[5],
      article_category: data[6],
      articles_categories_mm: data[7],
      genera_category: data[8],
      generas_categories_mm: data[9]
    }));
  }

  get dataKeys() {
    return this.props && [
      'genera',
      'art',
      'sort',
      'article',
      'media_image',
      'sort_mediaimage_mm',
      'article_categories',
      'articles_categories_mm',
      'genera_categories',
      'generas_categories_mm'
    ];
  }

  get descriptions() {
    if (this.precalced.descriptions) return this.precalced.descriptions;

    const descriptions = [];
    this.plantProps.forEach(
      descr => this.availableLanguages.includes(descr.sprache) && descriptions.push(descr)
    );

    this.precalced.descriptions = descriptions;
    return descriptions;
  }

  get genera() {
    if (this.precalced.genera) return this.precalced.genera;
    let genera = [];

    for (let lang = 0; lang < this.availableLanguages.length; lang++) {
      genera = genera.concat(getGenera(this.plants, this.descriptions, lang));
    }
    genera = findDuplicates(genera);
    genera = setGeneraIds(genera);
    
    this.precalced.genera = genera;
    return genera;
  }

  get generaAsync() {
    return promise(this.genera);
  }

  get generaCategories() {
    if (this.precalced.generaCategories) return this.precalced.generaCategories;
    let generaCategories = [];

    for (let lang = 0; lang < this.availableLanguages.length; lang++) {
      generaCategories = generaCategories.concat(getGeneraCategories(this.genera, this.articles, lang));
    }
    generaCategories = setGeneraCategoriesIds(generaCategories);

    this.precalced.generaCategories = generaCategories;
    return generaCategories;
  }

  get generaCategoriesAsync() {
    let generaCategories = JSON.parse(JSON.stringify(this.generaCategories));
    generaCategories = generaCategories.map(cat => {
      delete cat.generas;
      return cat;
    });
    return promise(generaCategories);
  }

  get generasCategoriesMM() {
    return setGenerasCategoriesMM(this.genera, this.generaCategories);
  }

  get generasCategoriesMMAsync() {
    return promise(this.generasCategoriesMM);
  }

  get imageSrc() {
    return this.props.imageSrc;
  }

  get images() {
    return globAsync(this.imageSrc)
      .then(files => getImages(files));
  }

  get qualities() {
    if (this.precalced.qualities) return this.precalced.qualities;
    const qualities = [];
    this.plants.forEach(plant => {
      const quality = plant.qualitaet_de && plant.qualitaet_de.replace(/ +(?= )/g,'');
      if (quality && qualities.indexOf(quality) === -1) {
        qualities.push(quality);
      }
    });
    
    this.precalced.qualities = qualities;
    return qualities;
  }

  get sorts() {
    return assignMinMaxValues(this.sortsWithoutMinMax, this.articles);
  }

  get sortImageMapping() {
    return Promise.all([
      this.images,
      this.sortsAsync
    ]).then(data => sortImageMapping(data));
  }

  get sortsAsync() {
    return promise(this.sorts);
  }

  get sortsWithoutMinMax() {
    if (this.precalced.sortsWithoutMinMax) return this.precalced.sortsWithoutMinMax;
    let sorts = [];

    for (let lang = 0; lang < this.availableLanguages.length; lang++) {
      sorts = sorts.concat(getSorts(this.plants, this.descriptions, lang));
    }
    sorts = setSortIds(sorts, this.arts);

    this.precalced.sortsWithoutMinMax = sorts;
    return sorts;
  }

  get plants() {
    return this.props.plants;
  }

  get plantProps() {
    return this.props.plantProps;
  }

  get plantQualityCategories() {    
    return this.props.plantQualityCategories;
  }
}

module.exports = PlantImport;

