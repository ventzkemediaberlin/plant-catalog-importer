const path = require('path');

const src = path.resolve(__dirname, '..', 'src');

// const plants = require('../src/catalog.json');
// const plantProps = require('../src/catalog-properties.json');

const imageSrc = path.resolve(src, 'images', '**', '*.jpg');

const availableLanguages = ['DE', 'EN', 'RU'];

const options = {
  availableLanguages,
  imageSrc,
  // plants,
  // plantProps,
};

module.exports = options;