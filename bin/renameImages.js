
/* jshint esversion: 6 */
/* jshint node: true */

'use strict';


const {readdirSync, rename} = require('fs');
const path = require('path');


const renameImages = () => {

    const src = path.resolve(__dirname, '..');

    // const plants = require('../src/catalog.json');
    // const plantProps = require('../src/catalog-properties.json');
    
    const imageDirPath = path.resolve(src,'dist', 'forms'); //TODO resolve path to all images with **

    // Get path to image directory
    console.log(imageDirPath);
  
    // Get an array of the files inside the folder
    const files = readdirSync(imageDirPath);
    // console.log(files)
    
    // Loop through each file that was retrieved
    files.forEach(filename => {
      const oldPath = `${imageDirPath}/${filename}`;
      // console.log('oldPath', oldPath);
  
      const regex = /[!@#$%^&*(),?'":{}| <>]/g;
      while ((regex.exec(filename)) !== null) {
        filename = filename.replace(regex, '_');
        filename = filename.replace(/JPG/g, 'jpg');
  
      }
      const newPath = `${imageDirPath}/${filename}`;
      // console.log('newPath', newPath);
  
      rename(oldPath, newPath, err => {
        console.log('err', err);
      });
    });
  };
  
  module.exports = {
    renameImages
  };
  




