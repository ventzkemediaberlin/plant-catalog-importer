const csvjson = require('csvjson');
const fs = require('fs')
const path = require('path');

const argv = process.argv;
const isProduction = argv.indexOf('--prod') > -1;

const loopback = require('loopback');
const app = loopback();
const _ = require('lodash')
let http = require('http');
const axios = require('axios')
const Bottleneck = require('bottleneck');

const limiter = new Bottleneck({
    minTime: 200,
    maxConcurrent: 5
});




'use strict';

//TODO ADD succesful & failed count

const createSan = () => {

    const data = fs.readFileSync(path.join(__dirname, '..', 'dist', 'test_short.csv'), { encoding: 'utf8' });

    var options = {
        delimiter: ';', 
        quote: '"' 
    };

    let succesfulCount = 0;
    let failedCount = 0;


    const json = csvjson.toObject(data, options);

    const getData = (item, sanNr) => {

        const res = axios.get(`http://localhost:1338/api/articles?filter={"where":{"bdbnr":"${item['BDBNR']}"}}`)
            .then(res => postCall(res.data, sanNr))
            .catch(err => {
                console.log(err.code)
            })
    }

    const throttledGetData = limiter.wrap(getData);

    async function getAllResults() {

        const allThePromises = json.map(item => {
            if (item['BDBNR'].length > 0) {

                let sanNr;
                let sanPartOne;
                let sanPartTwo;
                let sanPartThree;

                if (item['BDB_SORTENTEXT_NR'].length === 5) {
                    sanPartOne = (item['BDB_SORTENTEXT_NR']);
                } else {
                    failedCount++;
                    return new Error('BDB_SORTENTEXT_NR incorrect');
                }
                if (item['SAN_MITTE'].length === 12) {
                    sanPartTwo = item['SAN_MITTE'];
                } else if (item['SAN_MITTE'].length === 0) {
                    sanPartTwo = '000000000000';
                } else {
                    failedCount++;
                    return new Error('SAN_Mitte incorrent');
                }
                if (item['SAN_ENDE'].length === 12) {
                    sanPartThree = item['SAN_ENDE'];
                } else if (item['SAN_ENDE'].length !== 12) {
                    sanPartThree = item['SAN_ENDE'];
                    for (let i = sanPartThree.length; i < 12; i++) {
                        sanPartThree = '0'.concat(sanPartThree);
                    }
                }

                sanNr = sanPartOne + sanPartTwo + sanPartThree;


                if (sanNr.length !== 29) {
                    failedCount++;
                    return new Error('SAN Nummer incorrect');
                }

                throttledGetData(item, sanNr)
            }
        })


        try {
            const results = await Promise.all(allThePromises);
        }
        catch (err) {
            console.log(err);
        }
    }


    getAllResults();

    const postCall = (data, sanNr) => {
        if (data.totalRowCount > 0 && data.rows[0]) {


            if (data.rows[0].bdbnr.length > 0) {
                data.rows[0].bdbnr = sanNr;
                data.rows[0].tstamp = 1;
                data.rows[0].crdate = 1;
                data = data.rows[0];

                const postData = async () => {
                    try {
                        console.log('fired')
                        const res = await axios.post(`http://localhost:1338/api/articles/${data.id}/replace`, data)
                            .then(res => {
                                console.log(`Status: ${res.status}`);
                                console.log(typeof(res.status));
                                console.log('Body: ', res.data.bdbnr);
                                succesfulCount++;
                            })
                    } catch (err) {
                        console.log(err.code)
                        failedCount++
                    }
                }

                postData();
            }
        }
    }

    console.log('succesfulCount', succesfulCount)
    console.log('failedCount', failedCount)

}



createSan();


