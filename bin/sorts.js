const options = require('./options');

const getSorts = (plants, descriptions, lang) => {
  if (!plants) {
    return new Error('Can\'t find any plants');
  }
  if (!descriptions) {
    return new Error('Can\'t find any description');
  }
  const sorts = [];
  plants.forEach(plant => {
    const description = descriptions && descriptions.find(descr => descr.bdb_sorten_nr === plant.bdb_sortentext_nr && descr.sprache === options.availableLanguages[lang]);
    const hasSort = sorts.find(sort => sort.oid === plant.bdb_sortentext_nr); 

    const name1 = 
      (description && description.bot_name_katalog) || 
      plant.botname;

    const name2 = 
      (description && description.landesname) ||
      (lang === 0 && plant.landesname_de) || 
      (lang === 1 && plant.landesname_en) || 
      (lang === 2 && plant.landesname_ru) || 
      plant.gattung_art;

    if (!hasSort && plant.web) {
      const sort = Object.assign({}, {
        oid: plant.bdb_sortentext_nr,
        name1,
        name2,
        matchcode: plant.sorten_matchcode,
        annotation: description && description.anmerkung || null,
        claim: description && description.ansprueche || null,
        leaf: description && description.blatt || null,
        blossom: description && description.bluete || null,
        width: description && description.breite || null,
        family: description && description.familie || null,
        fruit: description && description.frucht || null,
        habitus: description && description.habitus || null,
        height: description && description.hoehe || null,
        category: description && description.kategorie || null,
        climate_zone: description && description.klimazone || null,
        usage: description && description.verwendung,
        growth: description && description.wuchs || null,
        sort_recommendation: null,
        deleted: false,
        hidden: true,
        light_demanding: description && description.lichtanspruch || null,
        video: description && description.sortenvideo_link || null,
        ancestry: description && description.herkunft || null,
        fragrance: description && description.duft || null,
        minwidth: null,
        maxwidth: null,
        minheight: null,
        maxheight: null,
        minrange: null,
        maxrange: null,
        minprice: null,
        maxprice: null,
        lang,
        __artOid: plant.gattung_art_nr,
        __generaOid: plant.gattung_nr,
      });
      
      // Showing sort if has any descriptions...
      const descriptions = ['growth', 'usage', 'climate_zone', 'height', 'habitus', 'fruit', 'family', 'width', 'blossom', 'claim', 'annotation'];
      for (desc of descriptions) {
        if (sort[desc]) {
          sort.hidden = false;
        }
      }

      sorts.push(sort);
    }
  });
  return sorts;
};

const getAbsoluteMinMax = (props) => {
  const resolveMax = (values) => {
    if (!values || values.length === 0) {
      return null;
    }
    let current;
    values.forEach(val => {
      if (!current || current < val) {
        current = val;
      }
    });
    return current;
  };
  const resolveMin = (values) => {
    if (!values || values.length === 0) {
      return null;
    }
    let current;
    values.forEach(val => {
      if (!current || current > val) {
        current = val;
      }
    });
    return current;
  };
  return {
    min: resolveMin(props.min),
    max: resolveMax(props.max),
  };
};

const assignMinMaxValues = (sorts, articles) => {
  sorts.forEach((sort, index) => {
    const relatedArticles = articles.filter(a => a.sortId === sort.id);
    if (relatedArticles && relatedArticles.length > 0) {
      const height = {
        min: [],
        max: [],
      };
      const width = {
        min: [],
        max: [],
      };
      const range = {
        min: [],
        max: [],
      };
      const price = {
        min: [],
        max: [],
      };
      relatedArticles.map(a => {
        if (a.minheight && a.minheight > 0) {
          height.min.push(a.minheight);
        }
        if (a.maxheight && a.maxheight > 0) {
          height.max.push(a.maxheight);
        }
        if (a.minwidth && a.minwidth > 0) {
          width.min.push(a.minheight);
        }
        if (a.maxwidth && a.maxwidth > 0) {
          width.max.push(a.maxheight);
        }
        if (a.maxwidth && a.maxwidth > 0) {
          width.max.push(a.maxheight);
        }
        if (a.mintreetrunkscope && a.mintreetrunkscope > 0) {
          range.min.push(a.mintreetrunkscope);
        }
        if (a.maxtreetrunkscope && a.maxtreetrunkscope > 0) {
          range.max.push(a.maxtreetrunkscope);
        }
        if ((a.netto1 && a.netto1 > 0) || (a.netto2 && a.netto2 > 0)) {
          price.min.push(netto1);
          price.min.push(netto2);
          price.max.push(netto1);
          price.max.push(netto2);
        }
      });
      sort = Object.assign({}, sort, {
        minwidth: getAbsoluteMinMax(width).min,
        maxwidth: getAbsoluteMinMax(width).max,
        minheight: getAbsoluteMinMax(height).min,
        maxheight: getAbsoluteMinMax(height).max,
        minrange: getAbsoluteMinMax(range).min,
        maxrange: getAbsoluteMinMax(range).max,
        minprice: getAbsoluteMinMax(price).min,
        maxprice:  getAbsoluteMinMax(price).max,
      });
    }
  });

  return deactivateSorts(sorts, articles);
};

const setSortIds = (sorts, arts) => {
  if (!arts) {
    return new Error('Can\'t find any arts');
  }
  if (!sorts) {
    return new Error('Can\'t find any sorts');
  }
  
  sorts.forEach((sort, index) => {
    const art = arts.find(a => a.oid === sort.__artOid && a.lang === sort.lang);
    if (art) {
      sorts[index] = Object.assign({}, sort, {
        id: index + 1,
        artId: art.id,
        generaId: art.generaId,
      });
      
      delete sorts[index].__artOid;
      delete sorts[index].__generaOid;
    }
  });
  return sorts;
};

const deactivateSorts = (sorts, articles) => {
  sorts.map(sort => {
    const article = articles.find(art => art.sortId === sort.id);
    const hasArticles = article ? true : false;
    if (!hasArticles) {
      sort.hidden = true;
    }
  });
  return sorts;
};

module.exports = {
  assignMinMaxValues,
  deactivateSorts,
  getSorts,
  setSortIds,
};