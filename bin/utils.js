const glob = require('glob');

const globAsync = (fileGlob, options = {}) => 
  new Promise((resolve, reject) => {
    glob(fileGlob, options, (err, files) => {
      if (err) {
        reject(err);
      } else {
        resolve(files);
      }
    });
  });

const promise = props => 
  new Promise((resolve, reject) => {
    if (props instanceof Error) {
      reject(props);
    } else {
      resolve(props);
    }
  });

function twoDigits(d) {
  if (0 <= d && d < 10) return '0' + d.toString();
  if (-10 < d && d < 0) return '-0' + (-1 * d).toString();
  return d.toString();
}

Date.prototype.toMysqlFormat = function() {
  return this.getUTCFullYear() +
    '-' +
    twoDigits(1 + this.getUTCMonth()) +
    '-' +
    twoDigits(this.getUTCDate()) +
    ' ' +
    twoDigits(this.getUTCHours()) +
    ':' +
    twoDigits(this.getUTCMinutes()) +
    ':' +
    twoDigits(this.getUTCSeconds());
};

const assignTimeStamps = (obj) => Object.assign({}, obj, {
  created_at: new Date(Date.now()).toMysqlFormat(),
  updated_at: new Date(Date.now()).toMysqlFormat(),
});

module.exports = {
  assignTimeStamps,
  globAsync,
  promise,
};