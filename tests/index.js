const PlantImport = require('../bin');
const csv2json = require('lorberg-csv-import');

csv2json({json: true})
  .then(data => {
    const {
      catalog, 
      languages,
    } = data;

    // console.log(Object.keys(data));
    const importer = new PlantImport({
      plants: catalog,
      plantProps: languages,
      lang: 'EN'
    });

    // console.log(languages[1]);
    // console.log(languages[111]);
    
    // console.log(importer.sorts[0]);

  })
  .catch(error => console.log(error));